window.onload = function() {

  var noScroll= function(){
    window.scrollTo(0,1300);
  }
  var videos = document.getElementsByClassName('video')[0];
  var clickfun= function(){
    var videodiv=this;
    var src=videodiv.childNodes[1].childNodes[1].src;
    console.log(src)
    var modalvideo = document.getElementById('video01')
    modalvideo.src=src;
    console.log(modalvideo.src)
    var vidmodal=document.getElementsByClassName('modal')[0];
    vidmodal.classList.add('display')
    window.addEventListener('scroll', noScroll);
  }
  videos.addEventListener('click',clickfun);

  var remove=document.getElementsByClassName('close')[0];
  var myfun=function() {
    var display=document.getElementsByClassName('modal')[0];
    display.classList.remove('display');
    window.removeEventListener('scroll', noScroll);
  }
  remove.addEventListener('click',myfun);


  var speakersdetails =[
    {
      name:"enrique pena nieto",
      title:"president, mexico",
      details:"Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis hic voluptatum esse natus suscipit sunt at quas nobis mollitia eos saepe accusamus sequi temporibus nostrum quibusdam, magni architecto repellat laudantium.1"
    },
    {
      name:"gudni johanneon",
      title:"president, iceland",
      details:"Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis hic voluptatum esse natus suscipit sunt at quas nobis mollitia eos saepe accusamus sequi temporibus nostrum quibusdam, magni architecto repellat laudantium.2"
    },
    {
      name:"luis guillermo solís",
      title:"president, costa rica",
      details:"Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis hic voluptatum esse natus suscipit sunt at quas nobis mollitia eos saepe accusamus sequi temporibus nostrum quibusdam, magni architecto repellat laudantium.3"
    },
    {
      name:"josé calzada rovirosa",
      title:"secretary of agriculture, mexico",
      details:"Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis hic voluptatum esse natus suscipit sunt at quas nobis mollitia eos saepe accusamus sequi temporibus nostrum quibusdam, magni architecto repellat laudantium.4"
    },
    {
      name:"eneida de león",
      title:"minister of housing, territorial planning and environment, Uruguay",
      details:"Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis hic voluptatum esse natus suscipit sunt at quas nobis mollitia eos saepe accusamus sequi temporibus nostrum quibusdam, magni architecto repellat laudantium.5"
    },
    {
      name:"adisorn promthep",
      title:"director general of fisheries,thailand",
      details:"Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis hic voluptatum esse natus suscipit sunt at quas nobis mollitia eos saepe accusamus sequi temporibus nostrum quibusdam, magni architecto repellat laudantium.6"
    },
    {
      name:"zanny minton beddoes",
      title:"editor-in-cheif, the economist",
      details:"Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis hic voluptatum esse natus suscipit sunt at quas nobis mollitia eos saepe accusamus sequi temporibus nostrum quibusdam, magni architecto repellat laudantium.7"
    },
    {
      name:"zanny minton beddoes",
      title:"regional director latin america, economist intelligence unit",
      details:"Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis hic voluptatum esse natus suscipit sunt at quas nobis mollitia eos saepe accusamus sequi temporibus nostrum quibusdam, magni architecto repellat laudantium.8"
    }
  ]

  var speakers = document.querySelectorAll('.speakers li')
  var modal1 = document.getElementsByClassName('modal-speaker')[0];
  var noScrollimg= function(){
    window.scrollTo(0,2000);
  }
  var getdetails = function (){
    var speak=this;
    var img = speak.childNodes[1].childNodes[1].src;
    var photo= document.getElementById('img1');
    var namedetails = speak.childNodes[3].innerHTML;
    var titledetails = speak.childNodes[5].innerHTML;
    for (var i=0; i<speakersdetails.length; i++) {
      names = speakersdetails[i].name;
      titles = speakersdetails[i].title; 
      if (names===namedetails && titles===titledetails) {
        var head = document.createElement('h5')
        var para = document.createElement('p')
        var headtext = document.createTextNode(namedetails);
        var paratext = document.createTextNode(titledetails);
        var detail = document.createElement('p');
        var detailnode= document.createTextNode(speakersdetails[i].details)
        detail.appendChild(detailnode);
        head.appendChild(headtext);
        para.appendChild(paratext);
        modal1.appendChild(head);
        modal1.appendChild(para);
        modal1.appendChild(detail);
        photo.src=img;
        window.addEventListener('scroll', noScrollimg);
      }
    }
    modal1.classList.add('speakersdisplay')
  }

  for (var i=0; i < speakers.length; i++)
  speakers[i].addEventListener('click',getdetails,true)

  var remove1=document.getElementsByClassName('close')[1];
  var removefun=function() {
    var display=document.getElementsByClassName('modal-speaker')[0];
    display.classList.remove('speakersdisplay');
    window.removeEventListener('scroll', noScrollimg);
    var removeh = document.querySelector('.modal-speaker h5')
    var removepara = document.querySelectorAll('.modal-speaker p')
    for (var z=0; z <removepara.length; z++){
      modal1.removeChild(removepara[z])
    }
    modal1.removeChild(removeh)

    }
    remove1.addEventListener('click',removefun);

    var removebody2=document.getElementsByClassName('modal-speaker')[0];
    removebody2.addEventListener('click',removefun,true);
}